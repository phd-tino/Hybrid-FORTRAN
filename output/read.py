# Laidin Tino 2021

import numpy as np
import re
from os import walk
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def ReadData():
    scinot = re.compile('[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)')
    data_file = open("../run/data.dat", "r")
    line = data_file.readline() # xstar
    xstar = float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # vstar
    vstar = float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # T
    T = float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # epsilon
    epsilon = float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # Nx
    Nx = int(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # Nv
    Nv = int(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # C
    C = float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0])
    line = data_file.readline() # eta0
    eta0 = float(re.findall(scinot, line)[0])
    line = data_file.readline() # delta0
    delta0 = float(re.findall(scinot, line)[0])
    data_file.close()
    return(xstar, vstar, T, epsilon, Nx, Nv, C, eta0, delta0)

# Tools to sort files
def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def ReadKin(Directory, Nx, Nv):
    dir_dist  = Directory+"Distributions/"
    dir_dens  = Directory+"Densities/"
    dir_state = Directory+"StatesAndMass/"
    dir_indic = Directory+"Indicator/"
    Distribution_files  = next(walk(dir_dist), (None, None, []))[2]  # [] if no file
    Density_files       = next(walk(dir_dens), (None, None, []))[2]  # [] if no file
    StatesAndMass_files = next(walk(dir_state), (None, None, []))[2] # [] if no file
    Indicator_files     = next(walk(dir_indic), (None, None, []))[2] # [] if no file

    Distribution_files.sort(key=natural_keys)
    Density_files.sort(key=natural_keys)
    StatesAndMass_files.sort(key=natural_keys)
    Indicator_files.sort(key=natural_keys)
    
    ### Distributions
    Dist = np.zeros((len(Distribution_files), Nx, Nv))
    cpt  = 0
    for file in Distribution_files:
        Dist[cpt,:,:] = np.loadtxt(dir_dist+file, dtype=float)
        cpt += 1

    ### Densities
    data_file = open(dir_dens+Density_files[0], "r")
    line = data_file.readline()
    data_file.close()
    match_number = re.compile('-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?')
    final_list = [float(x) for x in re.findall(match_number, line)]
    dt = final_list[1]

    Dens = np.zeros((len(Density_files), Nx))
    date = []
    iterations = []
    cpt = 0
    for file in Density_files:
        step = float(re.findall(r'\d+', Density_files[cpt])[0])
        date.append(dt*step)
        iterations.append(step)
        Dens[cpt,:] = np.genfromtxt(dir_dens+file, usecols=0, dtype=float)
        cpt+=1

    ### States and mass
    tmp = np.full(((len(StatesAndMass_files), Nx)), True, dtype=str)
    mass  = []
    cpt = 0
    for file in StatesAndMass_files:
        data_file = open(dir_state+file, "r")
        line = data_file.readline()
        mass.append(float(re.findall(r"[-+]?\d*\.\d+|\d+", line)[0]))
        data_file.close()
        tmp[cpt,:] = np.genfromtxt(dir_state+file, usecols=0, dtype="|S10")
        cpt = cpt + 1
    State = np.full(((len(StatesAndMass_files), Nx)), True, dtype=bool)
    State = np.where(tmp=='T', True, False)
    mass=np.array(mass)

    ### Indicators
    Indic = np.zeros((len(Indicator_files), Nx))
    L1G = np.zeros((len(Indicator_files), Nx))
    cpt = 0
    for file in Indicator_files:
        Indic[cpt,:] = np.genfromtxt(dir_indic+file, usecols=0, dtype=float)
        L1G[cpt,:] = np.genfromtxt(dir_indic+file, usecols=1, dtype=float)
        cpt+=1
    
    return (Dist, Dens, State, mass, Indic, L1G, date)
    
def ReadLim(Nx):
    dir_dens  = "Limit/Densities/"
    Density_files = next(walk(dir_dens), (None, None, []))[2]  # [] if no file
    Density_files.sort(key=natural_keys)
    
    Dens = np.zeros((len(Density_files), Nx))
    date = []
    iterations = []
    cpt = 0
    for file in Density_files:
        Dens[cpt,:] = np.genfromtxt(dir_dens+file, usecols=0, dtype=float)
        cpt+=1
    return (Dens)

def ReadAP(directory, Nx, Nv):

    dir1  = directory+"/Dens1/"
    dir2  = directory+"/Dens2/"
    dir3  = directory+"/Dens3/"
    dir4  = directory+"/Dens4/"

    Density_files1      = next(walk(dir1), (None, None, []))[2]  # [] if no file
    Density_files2      = next(walk(dir2), (None, None, []))[2]  # [] if no file
    Density_files3      = next(walk(dir3), (None, None, []))[2]  # [] if no file
    Density_files4      = next(walk(dir4), (None, None, []))[2]  # [] if no file


    Density_files1.sort(key=natural_keys)
    Density_files2.sort(key=natural_keys)
    Density_files3.sort(key=natural_keys)
    Density_files4.sort(key=natural_keys)
    
    ### Densities
    data_file = open(dir1+Density_files1[0], "r")
    line = data_file.readline()
    data_file.close()
    match_number = re.compile('-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?')
    final_list = [float(x) for x in re.findall(match_number, line)]
    dt = final_list[1]

    Dens1 = np.zeros((len(Density_files1), Nx))
    Dens2 = np.zeros((len(Density_files2), Nx))
    Dens3 = np.zeros((len(Density_files3), Nx))
    Dens4 = np.zeros((len(Density_files4), Nx))
    
    date = []
    iterations = []
    cpt = 0
    for file in Density_files1:
        step = float(re.findall(r'\d+', Density_files1[cpt])[0])
        date.append(dt*step)
        iterations.append(step)
        Dens1[cpt,:] = np.genfromtxt(dir1+file, usecols=0, dtype=float)
        cpt+=1
        
    cpt = 0
    for file in Density_files2:
        Dens2[cpt,:] = np.genfromtxt(dir2+file, usecols=0, dtype=float)
        cpt+=1
        
    cpt = 0
    for file in Density_files3:
        Dens3[cpt,:] = np.genfromtxt(dir3+file, usecols=0, dtype=float)
        cpt+=1
        
    cpt = 0
    for file in Density_files4:
        Dens4[cpt,:] = np.genfromtxt(dir4+file, usecols=0, dtype=float)
        cpt+=1
    
    return (Dens1, Dens2, Dens3, Dens4, dt, date)