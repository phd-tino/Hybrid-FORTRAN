! Laidin Tino
module numerics
!
    ! Number representation
    integer, parameter :: sp = selected_real_kind(4)
    integer, parameter :: dp = selected_real_kind(8)
    ! integer, parameter :: qp = selected_real_kind(16)
    integer, parameter :: rp = dp
!   
    real(rp),  parameter  ::  zero = 0.0_rp
    real(rp),  parameter  ::  PI   = acos(-1.0_rp)
!
    ! Finite differences coefficients for 7 points stencils
    real(rp), dimension(-3:3), parameter :: &
        ! First derivative, order 6
        FDC1 = (/-1/60.0_rp, 3/20.0_rp, -0.75_rp, 0.0_rp, 0.75_rp, -3/20.0_rp, 1/60.0_rp/),&
        ! Second derivative, order 6
        FDC2 = (/1/90.0_rp, -3/20.0_rp, 1.5_rp, -49/18.0_rp, 1.5_rp, -3/20.0_rp, 1/90.0_rp/),&
        ! Third derivative, order 4
        FDC3 = (/0.125_rp, -1.0_rp, 13/8.0_rp, 0.0_rp, -13/8.0_rp, 1.0_rp, -0.125_rp/),&
        ! Fourth derivative, order 4
        FDC4 = (/-1/6.0_rp, 2.0_rp, -13/2.0_rp, 28/3.0_rp, -13/2.0_rp, 2.0_rp, -1/6.0_rp/)
!  
    ! Parameters of the problem
    real(rp) :: xstar, vstar, T, eps, inveps, m2, m4, massM, mass, massEq
    real(rp), dimension(:), allocatable :: E, Ehalf, Ehp, Ehm, D1xE, D2xE, D3xE,&
                                           P, Phalf, M, D1vM
    ! Poisson coupling is not considered therefore E is considered as a data
!    
    ! Discretization parameters
    integer  :: Nx, Nv, Nt
    real(rp) :: dx, invdx, dv, invdv, dt, invdt, dtsdx, dtsdv, dtseps2, cfl, C
    real(rp), dimension(:), allocatable :: X, Xhalf, V, Vp, Vm
    ! Some constants are precomputed to avoid divisions
!
    ! Coupling parameters
    logical  :: Coupling
    real(rp) :: eta0, delta0
    real(rp), dimension(:)  , allocatable :: indicator, L2G, D1xRho, D2xRho, D3xRho, D4xRho
    logical,  dimension(:)  , allocatable :: isKinetic, isKinetic_n
!
    ! Unknowns
    real(rp), dimension(:,:), allocatable :: F, Fn, G, Gn, Eq, invEq
    real(rp), dimension(:)  , allocatable :: rho, rhohalf, rho_n, rhohalf_n
    ! G, Gn, rhohalf and rhohalf_n are values at interfaces. 
    ! Periodic BC allows to identify the first and last interfaces.
    ! Therefore, they have the same dimensions as cell centers arrays.
    ! The first index corresponds to the first interface x_0.5
    ! G(1) = G(x_{0.5})
    ! G(i) = G(x_{i-0.5})
!
    ! IO
    integer :: nb_frames, freq, Method
    character(len=50) :: datafile, Output
    integer :: Initdata, Efielddata
!     
    ! Parallel
    integer :: chunk
!  
end module numerics