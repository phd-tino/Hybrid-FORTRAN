! Laidin Tino
module IO
!
      use numerics
!    
    contains
!
    ! --- INPUTS ---    
!    
    subroutine ReadData(filename)
        implicit none
        character(len=50), intent(in) :: filename
        character(len=50)             :: deco
        open(unit=60, file=filename)
        read(60,*) deco, xstar
        read(60,*) deco, vstar
        read(60,*) deco, T
        read(60,*) deco, eps
        read(60,*) deco, Nx
        read(60,*) deco, Nv
        read(60,*) deco, C
        read(60,*) deco, eta0
        read(60,*) deco, delta0
        close(60)
    end subroutine ReadData
!
    subroutine ChooseOutput()
        implicit none
        Nt = floor(T/dt)
        write(6,*) "|----------------------------------------|"
        write(6,*) "| Nt = T/dt = ", Nt
        write(6,*) "| How many frames to save ?              |"
        read(5,*) nb_frames
        freq = Nt/nb_frames
        write(6,*) "|----------------------------------------|"
        write(6,*) "|              Running ...               |"
    end subroutine ChooseOutput
!
    subroutine ChooseInitialData()
        implicit none
        write(6,*) "|----------------------------------------|"
        write(6,*) "|      Choose an initial data            |"
        write(6,*) "| 1 --> V4_Cos                           |"
        write(6,*) "| 2 --> Gaussian_Cos                     |"
        write(6,*) "| 3 --> Ball                             |"
        write(6,*) "| 4 --> Custom                           |"
        read(5,*) Initdata
    end subroutine ChooseInitialData
!
    subroutine ChooseEfield()
        implicit none
        write(6,*) "|----------------------------------------|"
        write(6,*) "|      Choose an electrical field        |"
        write(6,*) "| 1 --> Null                             |"
        write(6,*) "| 2 --> Cosine                           |"
        read(5,*) Efielddata
    end subroutine ChooseEfield
!
    ! --- OUTPUTS ---  
!
    subroutine Save_density(iter, frame)
        implicit none
        integer, intent(in) :: iter, frame
        integer :: i
        character(len=10) :: file_id1, file_id2 
        character(len=70) :: file_name

        write(file_id1, '(i0)') iter
        write(file_id2, '(i0)') frame

        file_name = trim(Output)//"Densities/Density_"&
                    // "_I" // trim(adjustl(file_id1))&
                    // "_F" // trim(adjustl(file_id2))
        open(unit = 50, file = trim(file_name), status="replace")
        write(50,*) rho(1), dt
        do i = 2, Nx
            write(50,*) rho(i)
        end do
        close(50)
    end subroutine Save_density
!
    subroutine Save_distribution(iter, frame)
        implicit none
        integer, intent(in) :: iter, frame
        integer :: i, j
        character(len=10) :: file_id1, file_id2 
        character(len=50) :: file_name

        write(file_id1, '(i0)') iter
        write(file_id2, '(i0)') frame

        file_name = trim(Output) // "Distributions/Dist_"&
                    // "_I" // trim(adjustl(file_id1))&
                    // "_F" // trim(adjustl(file_id2))
        open(unit = 50, file = trim(file_name), status="replace")
        do i = 1, Nx
          write(50,*) (G(i,j), j = 1, Nv)
        end do
        close(50)
    end subroutine Save_distribution
!
    subroutine Save_StateAndMass(iter, frame)
        implicit none
        integer, intent(in)  :: iter, frame
        integer :: i
        character(len=10) :: file_id1, file_id2 
        character(len=50) :: file_name

        write(file_id1, '(i0)') iter
        write(file_id2, '(i0)') frame

        file_name = trim(Output) // "StatesAndMass/State_"&
                    // "_I" // trim(adjustl(file_id1))&
                    // "_F" // trim(adjustl(file_id2))
        open(unit = 50, file = trim(file_name), status="replace")
        write(50,*) isKinetic(1), mass
        do i = 2, Nx
          write(50,*) isKinetic(i)
        end do
        close(50)
    end subroutine Save_StateAndMass
!
    subroutine Save_indicator(iter, frame)
        implicit none
        integer, intent(in) :: iter, frame
        integer :: i
        character(len=10) :: file_id1, file_id2 
        character(len=70) :: file_name

        write(file_id1, '(i0)') iter
        write(file_id2, '(i0)') frame

        file_name = trim(Output) // "Indicator/Indic_" &
                    // "_I" // trim(adjustl(file_id1))&
                    // "_F" // trim(adjustl(file_id2))
        open(unit = 50, file = trim(file_name), status="replace")
        do i = 1, Nx
            write(50,*) indicator(i), L2G(i)
        end do
        close(50)
    end subroutine Save_indicator
! 
end module IO