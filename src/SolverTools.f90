! Laidin Tino
module solvertools
!
      use numerics
      use IO
!
      abstract interface
        function CI(x, v) result(val)
          real(selected_real_kind(8)) :: x, v, val
        end function CI
      end interface
!
      abstract interface
        function EF(x) result(val)
          real(selected_real_kind(8)) :: x, val
        end function EF
      end interface
!
    abstract interface
      function Pot(x) result(val)
        real(selected_real_kind(8)) :: x, val
      end function Pot
    end interface
!
    contains
!
    subroutine discretizePhasePlane()
        implicit none
        integer :: i, j

        !$OMP do
        do i = 1, Nx
            X(i)     = (i-0.5_rp)*dx ! Cell centers
            Xhalf(i) = i*dx          ! Interfaces
        end do
        !$OMP end do

        !$OMP do
        do j = 1, Nv
            V(j)  = -vstar + (j-0.5_rp)*dv ! Cell centers
            Vp(j) = max(0.0_rp,V(j))
            Vm(j) = min(0.0_rp,V(j))
        end do
        !$OMP end do
    end subroutine discretizePhasePlane
!
    subroutine discretizePhasePlane_LIM()
        implicit none
        integer :: i, j

        !$OMP do
        do i = 1, Nx
            X(i)     = (i-0.5_rp)*dx ! Cell centers
            Xhalf(i) = i*dx          ! Interfaces
        end do
        !$OMP end do

        !$OMP do
        do j = 1, Nv
            V(j)  = -vstar + (j-0.5_rp)*dv ! Cell centers
        end do
        !$OMP end do

    end subroutine discretizePhasePlane_LIM
!
    ! --------------------------------------
    ! ------------ Initial data ------------
    ! --------------------------------------
!
    function V4_Cos(x, v) result(val)
        implicit none
        real(rp) :: x, v, val
        val = gaussian(v)*v**4.0_rp*(1.0_rp+cos(2.0_rp * PI * x / xstar))
        ! val = exp(-abs(v)**1.5_rp)*(1.0_rp+cos(2.0_rp * PI * x / xstar))/ sqrt(2.0_rp * PI)
        return
    end function V4_Cos
!
    function Gaussian_Cos(x, v) result(val)
        implicit none
        real(rp) :: x, v, val
        val = gaussian(v)*(1.0_rp+0.5*cos(2.0_rp * PI * x / xstar))
        return
    end function Gaussian_Cos
!
    function Ball(x, v) result(val)
        implicit none
        real(rp) :: x, v, val
        if ((x-0.5_rp*xstar)**2+(v*0.5_rp/vstar)**2 < 0.2_rp**2) then
            val = 1.0_rp
        else
            val = zero
        end if
        return
    end function Ball
!
    function Custom(x,v) result(val)
        implicit none
        real(rp) :: x, v, val
        val = gaussian(v-4.0_rp)*gaussian((x-PI*0.5_rp)/0.3_rp) &
        + gaussian(v)*gaussian((x-PI/2)/0.3_rp) &
        + gaussian(v+4.0_rp)*gaussian((x-PI/2)/0.3_rp) &
        + 0.1*gaussian(v)*v**4_rp*(1.0_rp+1.0_rp*cos(2_rp * PI * x / xstar))
        return
    end function Custom
!
    ! --------------------------------------
    ! ---------- Electrical field ----------
    ! --------------------------------------
!
    function Efield_Cos(x) result(val)
        implicit none
        real(rp) :: x, val
        val = 0.5_rp*cos(2.0_rp*PI*x/xstar)
        return
    end function Efield_Cos
!
    function Pot_Sin(x) result(val)
        implicit none
        real(rp) :: x, val
        val = -0.25_rp*sin(2.0_rp*x)   
    end function Pot_Sin
!
    function Efield_Null(x) result(val)
        implicit none
        real(rp) :: x, val
        val = x*zero
        return
    end function Efield_Null
!
    function Pot_Null(x) result(val)
        implicit none
        real(rp) :: x, val
        val = x*zero   
    end function Pot_Null
!
    ! --------------------------------------
    ! -------- Discrete Maxwellian ---------
    ! --------------------------------------
!
    function gaussian(v) result(val)
        implicit none
        real(rp) :: v, val
        val = exp(-v*v*0.5_rp) / sqrt(2.0_rp * PI)
        return
    end function gaussian
!
    subroutine Maxwellian()
        implicit none
        integer  :: j

        !$OMP single
        massM = zero
        m2   = zero
        m4   = zero
        !$OMP end single

        !$OMP do reduction(+:massM)
        do j = 1, Nv
            M(j) = gaussian(V(j))
            massM = massM + gaussian(V(j))*dv
        end do
        !$OMP end do

        !$OMP do reduction(+:m2,m4)
        do j = 1, Nv
            M(j) = M(j)/massM
            m2 = m2 + V(j)*V(j)*M(j)*dv
            m4 = m4 + V(j)*V(j)*V(j)*V(j)*M(j)*dv
        end do
        !$OMP end do
    end subroutine Maxwellian
!
    ! --------------------------------------
    ! ------------ Equilibrium -------------
    ! --------------------------------------
    subroutine Equilibrium()
        implicit none
        integer :: i,j

        do j = 1, Nv
            do i = 1, Nx
                Eq(i,j)    = M(j)!*exp(-Phalf(i))*mass
                invEq(i,j) = 1.0_rp / Eq(i,j)
            end do
        end do
    
    end subroutine Equilibrium

!
    ! -------------------------------------
    ! ---------- Initializations ----------
    ! -------------------------------------
!
    subroutine init_MM()
        implicit none
        integer :: i, j
        procedure(CI),  pointer :: Initial_Cond => null()
        procedure(EF),  pointer :: Efield => null()
        procedure(Pot), pointer :: Potential => null()
        real(rp) :: tmp

        !$OMP master
        call ChooseInitialData()
        if(Initdata==1)then
            Initial_Cond => V4_Cos
            else if(Initdata==2)then
            Initial_Cond => Gaussian_Cos
            else if(Initdata==3)then
            Initial_Cond => Ball
            else if(Initdata==4)then
            Initial_Cond => Custom
            else
            write(6,*) "Choice invalid, using GaussianCos"
            Initial_Cond => Gaussian_Cos
        end if
        call ChooseEfield()
        if(Efielddata==1)then
            Efield => Efield_Null
            Potential => Pot_Null
            else if(Efielddata==2)then
            Efield => Efield_Cos
            Potential => Pot_Sin
            else
            write(6,*) "Choice invalid, using Efield"
            Efield => Efield_Null
        end if
        !$OMP end master

        ! Constants
        chunk = NX / 10
        dx    = xstar / real(Nx, rp)
        invdx = 1.0_rp / dx
        dv    = 2.0_rp*vstar / real(Nv, rp)
        invdv = 1.0_rp / dv
        call discretizePhasePlane()

        ! Electrical field and initial state
        !$OMP do
        do i = 1, Nx
            E(i)     = Efield(X(i))
            Ehalf(i) = Efield(Xhalf(i))
            Ehp(i)   = 0.5_rp*(Ehalf(i)+abs(Ehalf(i)))
            Ehm(i)   = 0.5_rp*(Ehalf(i)-abs(Ehalf(i)))
            P(i)     = Potential(X(i))
            Phalf(i) = Potential(Xhalf(i))
            indicator(i) = 3.0_rp
            isKinetic(i) = .true.
            ! isKinetic(i) = (mod(i,3) .eq. 0)! .or. (mod(i,3) .eq. 1)
            isKinetic_n(i) = isKinetic(i)
        end do
        !$OMP end do

        ! Maxwellian
        call Maxwellian()

        call Comp_dt_Limit()
        tmp = dt
        call Comp_dt_MM()
        dt = min(dt, tmp)

        !$OMP master
        write(6,*) "|----------------------------------------|"
        write(6,'(A,ES21.6,A)') " | dx = ", dx
        write(6,'(A,ES21.6,A)') " | dt = ", dt
        write(6,'(A,ES21.6,A)') " | m2 = ", m2
        write(6,'(A,ES21.6,A)') " | m4 = ", m4
        call ChooseOutput()
        !$OMP end master

        ! More constants
        invdt = 1.0_rp / dt
        dtsdx = dt / dx
        dtsdv = dt / dv
        inveps = 1.0_rp /eps
        dtseps2 = dt*inveps*inveps

        call DerivativesEfield()

        !$OMP do collapse(2)
        do j = 1, Nv
            do i = 1, Nx
                F(i,j) = Initial_Cond(X(i), V(j))
            end do
        end do
        !$OMP end do

        call project()
        call Comp_rhohalf()
        rhohalf = rhohalf_n

        !$OMP single
        mass = zero
        !$OMP end single
        !$OMP do reduction(+:mass)
        do i = 1, Nx
            mass = mass + dx*rho(i)
        end do 
        !$OMP end do

        call Equilibrium()

        !$OMP do
        do j = 1, Nv
            G(1,j) = 0.5_rp*(F(Nx,j)+F(1,j)) - rhohalf_n(1)*M(j)
            do i = 2, Nx
                G(i,j) = 0.5_rp*(F(i-1,j)+F(i,j)) - rhohalf_n(i)*M(j)
            end do
        end do
        !$OMP end do

    end subroutine init_MM
!
    subroutine init_Limit()
        implicit none
        integer :: i, j
        procedure(CI), pointer :: Initial_Cond => null()
        procedure(EF), pointer :: Efield => null()
        real(rp) :: tmp

        !$OMP master
        call ChooseInitialData()
        if(Initdata==1)then
            Initial_Cond => V4_Cos
        else if(Initdata==2)then
            Initial_Cond => Gaussian_Cos
        else if(Initdata==3)then
            Initial_Cond => Ball
        else if(Initdata==4)then
            Initial_Cond => Custom
        else
            write(6,*) "Choice invalid, using GaussianCos"
            Initial_Cond => Gaussian_Cos
        end if
        call ChooseEfield()
        if(Efielddata==1)then
            Efield => Efield_Null
        else if(Efielddata==2)then
            Efield => Efield_Cos
        else
            write(6,*) "Choice invalid, using Efield"
            Efield => Efield_Null
        end if
        !$OMP end master

        ! Constants
        dx    = xstar / real(Nx, rp)
        invdx = 1.0_rp / dx
        dv    = 2.0_rp*vstar / real(Nv, rp)
        invdv = 1.0_rp / dv
        call discretizePhasePlane_LIM()

        ! Electrical field
        !$OMP do
        do i = 1, Nx
            E(i)     = Efield(X(i))
            Ehalf(i) = Efield(Xhalf(i))
        end do
        !$OMP end do

        ! Maxwellian
        call Maxwellian()

        call Comp_dt_Limit()
        tmp = dt
        call Comp_dt_MM()
        dt = min(dt, tmp)

        !$OMP master
        write(6,'(A,ES21.6,A)') " | dx = ", dx,"          |"
        write(6,'(A,ES21.6,A)') " | dt = ", dt,"          |"
        write(6,'(A,ES21.6,A)') " | m2 = ", m2,"          |"
        write(6,'(A,ES21.6,A)') " | m4 = ", m4,"          |"
        call ChooseOutput()
        !$OMP end master

        ! Constants involving dt
        invdt = 1.0_rp / dt
        dtsdx = dt / dx
        dtsdv = dt / dv

        ! Initial distribution
        !$OMP do collapse(2)
        do j = 1, Nv
            do i = 1, Nx
                F(i,j) = Initial_Cond(X(i), V(j))
            end do
        end do
        !$OMP end do

        call project()
        call Comp_rhohalf()

        !$OMP master
        deallocate(F)
        !$OMP end master
    end subroutine init_Limit
!
    ! --- CFL Conditions ---
!
    subroutine Comp_dt_MM() ! A revoir /!\/!\/!\/!\/!\/!\/!\/!\/!\
        implicit none
        real(rp) :: tmp
        tmp = maxval(abs(E))
        if(tmp<epsilon(tmp))then
            dt = C*(dx*dx+eps*dx/vstar)
        else
            dt = C*(dx*dx+eps*min(dx/vstar, dv/maxval(abs(E))))
        end if
        dt = 0.0001_rp
        ! dt = C*dx*dx
    end subroutine
!
    subroutine Comp_dt_Limit()
        implicit none
        real(rp) :: tmp
        tmp = maxval(abs(E))
        if(tmp<epsilon(tmp))then
            dt = C*dx*dx/m2
        else
            dt = C*min(dx*dx/m2, dx/tmp)
        end if
        dt = 0.0001_rp
        ! dt = C*dx*dx
    end subroutine
!
    ! -------------------------------------
    ! ----- Allocations/Deallocations -----
    ! -------------------------------------
    subroutine allocate_MM()
        implicit none
        ! 1D arrays
        allocate( X(1:Nx), Xhalf(1:Nx), V(1:Nv), Vp(1:Nv), Vm(1:Nv),&
                  rho(1:Nx), rho_n(1:Nx), rhohalf(1:Nx), rhohalf_n(1:Nx),&
                  isKinetic(1:Nx), isKinetic_n(1:Nx),&
                  E(1:Nx), Ehalf(1:Nx), Ehp(1:Nx), Ehm(1:Nx),&
                  P(1:Nx), Phalf(1:Nx),&
                  D1xE(1:Nx), D2xE(1:Nx), D3xE(1:Nx),&
                  M(1:Nv), D1vM(1:Nv),&
                  D1xRho(1:Nx), D2xRho(1:Nx), D3xRho(1:Nx), D4xRho(1:Nx),&
                  L2G(1:Nx), indicator(1:Nx))
        ! 2D arrays
        allocate( G(1:Nx,1:Nv), Gn(1:Nx,1:Nv), F(1:Nx,1:Nv),&
                  Eq(1:Nx,1:Nv), invEq(1:Nx,1:Nv))
    end subroutine allocate_MM
!
    subroutine allocate_Limit()
        implicit none
        ! 1D arrays
        allocate( X(1:Nx), Xhalf(1:Nx), E(1:Nx), Ehalf(1:Nx),&
                 M(1:Nv), V(1:Nv),&
                 rho(1:Nx), rho_n(1:Nx), rhohalf(1:Nx), rhohalf_n(1:Nx))
        ! 2D arrays
        allocate( F(1:Nx,1:Nv) )
    end subroutine allocate_Limit
!
    subroutine deallocate_MM()
        implicit none
        ! 1D arrays
        deallocate( X, Xhalf, V, Vp, Vm,&
                  rho, rho_n, rhohalf, rhohalf_n,&
                  isKinetic, isKinetic_n, &
                  E, Ehalf, Ehp, Ehm,&
                  P, Phalf,&
                  D1xE, D2xE, D3xE,&
                  M, D1vM,&
                  D1xRho, D2xRho, D3xRho, D4xRho,&
                  L2G, indicator)
        ! 2D arrays
        deallocate( G, Gn, F, Eq, invEq)
    end subroutine deallocate_MM
!
    subroutine deallocate_Limit()
        implicit none
        ! 1D arrays
        deallocate( X, Xhalf, E, Ehalf, M, V, rho, rho_n, rhohalf, rhohalf_n)
    end subroutine deallocate_Limit
!
    ! -------------------------------------
    ! ------- Discrete derivatives --------
    ! -------------------------------------
!
    ! Periodic BC in space
    function Comp_dx(arr) result(val)
        implicit none
        integer :: i
        real(rp), dimension(1:Nx) :: arr, val
        val(1) = invdx*(arr(Nx)-arr(2))
        !$OMP do
        do i = 2, Nx-1 ! >>>>> PARALLELISABLE <<<<<<
            val(i) = invdx*(arr(i+1)-arr(i-1))
        end do
        !$OMP end do
        val(Nx) = invdx*(arr(1)-arr(Nx-1))
        return
    end function Comp_dx
!
    ! Null flux BC in velocity
    function Comp_dv(arr) result(val)
        implicit none
        integer :: j
        real(rp), dimension(1:Nv) :: arr, val
        val(2) = zero
        !$OMP do
        do j = 2, Nv-1 ! >>>>> PARALLELISABLE <<<<<<
            val(j) = invdx*(arr(j+1)-arr(j-1))
        end do
        !$OMP end do
        val(Nv) = zero
        return
    end function Comp_dv
!
    ! Computes the derivatives of the electrical field (1,2,3)
    subroutine DerivativesEfield()
        implicit none
        integer :: i
        ! --- Left boundary (periodic BC) ---
        i = 1
        ! First derivative
        D1xE(i)  = (FDC1(-3)*E(Nx-3)+FDC1(-2)*E(Nx-1)+FDC1(-1)*E(Nx)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(i+1)+FDC1(2)*E(i+2)+FDC1(3)*E(i+3))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(Nx-2)+FDC2(-2)*E(Nx-1)+FDC2(-1)*E(Nx)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(i+1)+FDC2(2)*E(i+2)+FDC2(3)*E(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xE(i)= (FDC3(-3)*E(Nx-2)+FDC3(-2)*E(Nx-1)+FDC3(-1)*E(Nx)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(i+1)+FDC3(2)*E(i+2)+FDC3(3)*E(i+3))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = 2
        ! First derivative
        D1xE(i)  = (FDC1(-3)*E(Nx-1)+FDC1(-2)*E(Nx)+FDC1(-1)*E(i-1)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(i+1)+FDC1(2)*E(i+2)+FDC1(3)*E(i+3))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(Nx-1)+FDC2(-2)*E(Nx)+FDC2(-1)*E(i-1)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(i+1)+FDC2(2)*E(i+2)+FDC2(3)*E(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xE(i)= (FDC3(-3)*E(Nx-1)+FDC3(-2)*E(Nx)+FDC3(-1)*E(i-1)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(i+1)+FDC3(2)*E(i+2)+FDC3(3)*E(i+3))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = 3
        ! First derivative
        D1xE(i)  = (FDC1(-3)*E(Nx)+FDC1(-2)*E(i-2)+FDC1(-1)*E(i-1)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(i+1)+FDC1(2)*E(i+2)+FDC1(3)*E(i+3))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(Nx)+FDC2(-2)*E(i-2)+FDC2(-1)*E(i-1)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(i+1)+FDC2(2)*E(i+2)+FDC2(3)*E(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xE(i)= (FDC3(-3)*E(Nx)+FDC3(-2)*E(i-2)+FDC3(-1)*E(i-1)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(i+1)+FDC3(2)*E(i+2)+FDC3(3)*E(i+3))&
                  *invdx*invdx*invdx

        ! --- Core of the domaine ---
        !$OMP do
        do i = 4, Nx-3
            ! First derivative
            D1xE(i)  = (FDC1(-3)*E(i-3)+FDC1(-2)*E(i-2)+FDC1(-1)*E(i-1)+&
                      FDC1(0)*E(i)+&
                      FDC1(1)*E(i+1)+FDC1(2)*E(i+2)+FDC1(3)*E(i+3))&
                      *invdx
            ! Second derivative
            D2xE(i) = (FDC2(-3)*E(i-3)+FDC2(-2)*E(i-2)+FDC2(-1)*E(i-1)+&
                      FDC2(0)*E(i)+&
                      FDC2(1)*E(i+1)+FDC2(2)*E(i+2)+FDC2(3)*E(i+3))&
                      *invdx*invdx
            ! Third derivative
            D3xE(i)= (FDC3(-3)*E(i-3)+FDC3(-2)*E(i-2)+FDC3(-1)*E(i-1)+&
                      FDC3(0)*E(i)+&
                      FDC3(1)*E(i+1)+FDC3(2)*E(i+2)+FDC3(3)*E(i+3))&
                      *invdx*invdx*invdx
        end do
        !$OMP end do
        ! --- Right boundary (periodic BC) ---
        i = Nx-2
        ! First derivative
        D1xE(i)  = (FDC1(-3)*E(i-3)+FDC1(-2)*E(i-2)+FDC1(-1)*E(i-1)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(i+1)+FDC1(2)*E(i+2)+FDC1(3)*E(1))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(i-3)+FDC2(-2)*E(i-2)+FDC2(-1)*E(i-1)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(i+1)+FDC2(2)*E(i+2)+FDC2(3)*E(1))&
        *invdx*invdx
        ! Third derivative
        D3xE(i)= (FDC3(-3)*E(i-3)+FDC3(-2)*E(i-2)+FDC3(-1)*E(i-1)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(i+1)+FDC3(2)*E(i+2)+FDC3(3)*E(1))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = Nx-1
        ! First derivative
        D1xE(i) = (FDC1(-3)*E(i-3)+FDC1(-2)*E(i-2)+FDC1(-1)*E(i-1)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(i+1)+FDC1(2)*E(1)+FDC1(3)*E(2))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(i-3)+FDC2(-2)*E(i-2)+FDC2(-1)*E(i-1)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(i+1)+FDC2(2)*E(1)+FDC2(3)*E(2))&
                  *invdx*invdx
        ! Third derivative
        D3xE(i) = (FDC3(-3)*E(i-3)+FDC3(-2)*E(i-2)+FDC3(-1)*E(i-1)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(i+1)+FDC3(2)*E(1)+FDC3(3)*E(2))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = Nx
        ! First derivative
        D1xE(i) = (FDC1(-3)*E(i-3)+FDC1(-2)*E(i-2)+FDC1(-1)*E(i-1)+&
                  FDC1(0)*E(i)+&
                  FDC1(1)*E(1)+FDC1(2)*E(2)+FDC1(3)*E(3))&
                  *invdx
        ! Second derivative
        D2xE(i) = (FDC2(-3)*E(i-3)+FDC2(-2)*E(i-2)+FDC2(-1)*E(i-1)+&
                  FDC2(0)*E(i)+&
                  FDC2(1)*E(1)+FDC2(2)*E(2)+FDC2(3)*E(3))&
                  *invdx*invdx
        ! Third derivative
        D3xE(i) = (FDC3(-3)*E(i-3)+FDC3(-2)*E(i-2)+FDC3(-1)*E(i-1)+&
                  FDC3(0)*E(i)+&
                  FDC3(1)*E(1)+FDC3(2)*E(2)+FDC3(3)*E(3))&
                  *invdx*invdx*invdx
    end subroutine DerivativesEfield
!
    ! Computes the derivatives of the density (1,2,3,4)
    subroutine DerivativesDensity()
        implicit none
        integer :: i
        ! --- Left boundary (periodic BC) ---
        i = 1
        ! First derivative
        D1xRho(i)  = (FDC1(-3)*rho(Nx-3)+FDC1(-2)*rho(Nx-1)+FDC1(-1)*rho(Nx)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(i+1)+FDC1(2)*rho(i+2)+FDC1(3)*rho(i+3))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(Nx-2)+FDC2(-2)*rho(Nx-1)+FDC2(-1)*rho(Nx)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(i+1)+FDC2(2)*rho(i+2)+FDC2(3)*rho(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xRho(i)= (FDC3(-3)*rho(Nx-2)+FDC3(-2)*rho(Nx-1)+FDC3(-1)*rho(Nx)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(i+1)+FDC3(2)*rho(i+2)+FDC3(3)*rho(i+3))&
                  *invdx*invdx*invdx
        ! Third derivative
        D4xRho(i)= (FDC4(-3)*rho(Nx-2)+FDC4(-2)*rho(Nx-1)+FDC4(-1)*rho(Nx)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(i+1)+FDC4(2)*rho(i+2)+FDC4(3)*rho(i+3))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = 2
        ! First derivative
        D1xRho(i)  = (FDC1(-3)*rho(Nx-1)+FDC1(-2)*rho(Nx)+FDC1(-1)*rho(i-1)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(i+1)+FDC1(2)*rho(i+2)+FDC1(3)*rho(i+3))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(Nx-1)+FDC2(-2)*rho(Nx)+FDC2(-1)*rho(i-1)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(i+1)+FDC2(2)*rho(i+2)+FDC2(3)*rho(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xRho(i)= (FDC3(-3)*rho(Nx-1)+FDC3(-2)*rho(Nx)+FDC3(-1)*rho(i-1)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(i+1)+FDC3(2)*rho(i+2)+FDC3(3)*rho(i+3))&
                  *invdx*invdx*invdx
        ! Third derivative
        D4xRho(i)= (FDC4(-3)*rho(Nx-1)+FDC4(-2)*rho(Nx)+FDC4(-1)*rho(i-1)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(i+1)+FDC4(2)*rho(i+2)+FDC4(3)*rho(i+3))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = 3
        ! First derivative
        D1xRho(i)  = (FDC1(-3)*rho(Nx)+FDC1(-2)*rho(i-2)+FDC1(-1)*rho(i-1)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(i+1)+FDC1(2)*rho(i+2)+FDC1(3)*rho(i+3))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(Nx)+FDC2(-2)*rho(i-2)+FDC2(-1)*rho(i-1)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(i+1)+FDC2(2)*rho(i+2)+FDC2(3)*rho(i+3))&
                  *invdx*invdx
        ! Third derivative
        D3xRho(i)= (FDC3(-3)*rho(Nx)+FDC3(-2)*rho(i-2)+FDC3(-1)*rho(i-1)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(i+1)+FDC3(2)*rho(i+2)+FDC3(3)*rho(i+3))&
                  *invdx*invdx*invdx
        ! Fourth derivative
        D4xRho(i)= (FDC4(-3)*rho(Nx)+FDC4(-2)*rho(i-2)+FDC4(-1)*rho(i-1)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(i+1)+FDC4(2)*rho(i+2)+FDC4(3)*rho(i+3))&
                  *invdx*invdx*invdx

        ! --- Core of the domaine ---
        !$OMP do
        do i = 4, Nx-3
            ! First derivative
            D1xRho(i)  = (FDC1(-3)*rho(i-3)+FDC1(-2)*rho(i-2)+FDC1(-1)*rho(i-1)+&
                      FDC1(0)*rho(i)+&
                      FDC1(1)*rho(i+1)+FDC1(2)*rho(i+2)+FDC1(3)*rho(i+3))&
                      *invdx
            ! Second derivative
            D2xRho(i) = (FDC2(-3)*rho(i-3)+FDC2(-2)*rho(i-2)+FDC2(-1)*rho(i-1)+&
                      FDC2(0)*rho(i)+&
                      FDC2(1)*rho(i+1)+FDC2(2)*rho(i+2)+FDC2(3)*rho(i+3))&
                      *invdx*invdx
            ! Third derivative
            D3xRho(i)= (FDC3(-3)*rho(i-3)+FDC3(-2)*rho(i-2)+FDC3(-1)*rho(i-1)+&
                      FDC3(0)*rho(i)+&
                      FDC3(1)*rho(i+1)+FDC3(2)*rho(i+2)+FDC3(3)*rho(i+3))&
                      *invdx*invdx*invdx
            ! Fourth derivative
            D4xRho(i)= (FDC4(-3)*rho(i-3)+FDC4(-2)*rho(i-2)+FDC4(-1)*rho(i-1)+&
                      FDC4(0)*rho(i)+&
                      FDC4(1)*rho(i+1)+FDC4(2)*rho(i+2)+FDC4(3)*rho(i+3))&
                      *invdx*invdx*invdx
        end do
        !$OMP end do
        ! --- Right boundary (periodic BC) ---
        i = Nx-2
        ! First derivative
        D1xRho(i)  = (FDC1(-3)*rho(i-3)+FDC1(-2)*rho(i-2)+FDC1(-1)*rho(i-1)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(i+1)+FDC1(2)*rho(i+2)+FDC1(3)*rho(1))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(i-3)+FDC2(-2)*rho(i-2)+FDC2(-1)*rho(i-1)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(i+1)+FDC2(2)*rho(i+2)+FDC2(3)*rho(1))&
        *invdx*invdx
        ! Third derivative
        D3xRho(i)= (FDC3(-3)*rho(i-3)+FDC3(-2)*rho(i-2)+FDC3(-1)*rho(i-1)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(i+1)+FDC3(2)*rho(i+2)+FDC3(3)*rho(1))&
                  *invdx*invdx*invdx
        ! Fourth derivative
        D4xRho(i)= (FDC4(-3)*rho(i-3)+FDC4(-2)*rho(i-2)+FDC4(-1)*rho(i-1)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(i+1)+FDC4(2)*rho(i+2)+FDC4(3)*rho(1))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = Nx-1
        ! First derivative
        D1xRho(i) = (FDC1(-3)*rho(i-3)+FDC1(-2)*rho(i-2)+FDC1(-1)*rho(i-1)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(i+1)+FDC1(2)*rho(1)+FDC1(3)*rho(2))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(i-3)+FDC2(-2)*rho(i-2)+FDC2(-1)*rho(i-1)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(i+1)+FDC2(2)*rho(1)+FDC2(3)*rho(2))&
                  *invdx*invdx
        ! Third derivative
        D3xRho(i) = (FDC3(-3)*rho(i-3)+FDC3(-2)*rho(i-2)+FDC3(-1)*rho(i-1)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(i+1)+FDC3(2)*rho(1)+FDC3(3)*rho(2))&
                  *invdx*invdx*invdx
        ! Fourth derivative
        D4xRho(i) = (FDC4(-3)*rho(i-3)+FDC4(-2)*rho(i-2)+FDC4(-1)*rho(i-1)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(i+1)+FDC4(2)*rho(1)+FDC4(3)*rho(2))&
                  *invdx*invdx*invdx
        ! ----------------------------
        i = Nx
        ! First derivative
        D1xRho(i) = (FDC1(-3)*rho(i-3)+FDC1(-2)*rho(i-2)+FDC1(-1)*rho(i-1)+&
                  FDC1(0)*rho(i)+&
                  FDC1(1)*rho(1)+FDC1(2)*rho(2)+FDC1(3)*rho(3))&
                  *invdx
        ! Second derivative
        D2xRho(i) = (FDC2(-3)*rho(i-3)+FDC2(-2)*rho(i-2)+FDC2(-1)*rho(i-1)+&
                  FDC2(0)*rho(i)+&
                  FDC2(1)*rho(1)+FDC2(2)*rho(2)+FDC2(3)*rho(3))&
                  *invdx*invdx
        ! Third derivative
        D3xRho(i) = (FDC3(-3)*rho(i-3)+FDC3(-2)*rho(i-2)+FDC3(-1)*rho(i-1)+&
                  FDC3(0)*rho(i)+&
                  FDC3(1)*rho(1)+FDC3(2)*rho(2)+FDC3(3)*rho(3))&
                  *invdx*invdx*invdx
        ! Fourth derivative
        D4xRho(i) = (FDC4(-3)*rho(i-3)+FDC4(-2)*rho(i-2)+FDC4(-1)*rho(i-1)+&
                  FDC4(0)*rho(i)+&
                  FDC4(1)*rho(1)+FDC4(2)*rho(2)+FDC4(3)*rho(3))&
                  *invdx*invdx*invdx
    end subroutine DerivativesDensity
!
    ! Project the distribution function to obtain the density
    subroutine project()
        implicit none
        integer :: i, j
        !$OMP do
        do i = 1, Nx ! >>>>> PARALLELISABLE <<<<<<
            rho(i) = zero
            do j = 1, Nv
                rho(i) = rho(i) + F(i,j)*dv
            end do
        end do
        !$OMP end do
    end subroutine project
!
    ! Compute the density at interfaces
    subroutine Comp_rhohalf()
        implicit none
        integer :: i
        rhohalf_n(1) = 0.5_rp*(rho(Nx) + rho(1))
        !$OMP do
        do i = 2, Nx
            rhohalf_n(i) = 0.5_rp*(rho(i-1) + rho(i))
        end do
        !$OMP end do    
    end subroutine Comp_rhohalf
!
end module solvertools