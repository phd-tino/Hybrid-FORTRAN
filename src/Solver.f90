! Laidin Tino
module hybridsolver
!
    use numerics
    use io
    use solvertools
    use hybridtools
!    
    contains
!
    ! -------------------------------------
    ! ---------- One time-step ------------
    ! -------------------------------------
!
    subroutine OneTimeStep_Hybrid_MM()
        implicit none
        integer  :: i, j
        real(rp), dimension(1:Nx+1) :: RhoFlux
        real(rp), dimension(1:Nv) :: Gl, Gr, FluxVel, FluxPos, phi
        real(rp) :: momphi, momvGn

        ! ----------------------------------------------------------------
        ! ---- First, deal with the pertubation g  and fluxes for rho ----
        ! ----------------------------------------------------------------
        ! Left Boundary, i = 1, interface 0.5
        if(isKinetic(1))then ! >>> Use the kinetic model
            ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\

            ! Only Check the first neighboors as sub-domains cannot be of size 1
            ! 1) Check the state of the left neighboor and adapt the value of Gl
            if(isKinetic(Nx))then
                Gl = G(Nx,:) ! G_i-1.5
            else
                Gl = zero ! Voir reconstruction ici ? /!\/!\/!\/!\
            end if
            ! 2) Check the state of the right neighboor and adapt the value of Gr
            if(isKinetic(2))then
                Gr = G(2,:) ! G_i+0.5
            else
                Gr = zero ! Voir reconstruction ici ? /!\/!\/!\/!\
            end if

            ! 3) Compute the kinetic micro flux
            ! 3.1) The velocity flux and moments needed for the position flux
            FluxVel(1) = (Ehm(1)*G(1,1) + Ehp(1)*G(1,2)&
                        +0.5_rp*Ehalf(1)*rhohalf(1)*(M(2)+M(1)))*dx ! Zero boundary flux in velocity
            phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(1,1) - Vm(1)*G(1,1) - Vp(1)*Gl(1))
            momphi = zero
            momphi = momphi + phi(1)*dv
            do j = 2, Nv-1
                ! Directly compute the sum of the fluxes
                ! Upwindflux
                FluxVel(j) = (Ehm(1)*G(1,j+1) + Ehp(1)*G(1,j) - Ehm(1)*G(1,j) - Ehp(1)*G(1,j-1)&
                            +0.5_rp*Ehalf(1)*rhohalf(1)*(M(j+1)-M(j-1)))*dx
                phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(1,j) - Vm(j)*G(1,j) - Vp(j)*Gl(j))
                momphi = momphi + phi(j)*dv
            end do
            FluxVel(Nv) = (Ehm(1)*G(1,Nv-1) + Ehp(1)*G(1,Nv)&
                         +0.5_rp*Ehalf(1)*rhohalf(1)*(M(Nv)+M(Nv-1)))*dx ! Zero boundary flux in velocity
            phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(1,Nv) - Vm(Nv)*G(1,Nv) - Vp(Nv)*Gl(Nv))
            momphi = momphi + phi(Nv)*dv

            ! 3.2) The position flux
            do j = 1, Nv
                ! Upwind flux
                FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*(rho(1)-rho(Nx)) )*dv
            end do

            ! 4) Update Gn at interface i-0.5 for all j and compute its moment
            momvGn = zero
            do j = 1, Nv
                Gn(1,j) = G(1,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                        *invdx*invdv*(FluxPos(j) + FluxVel(j))

                ! Gn(1,j) = G(1,j)*eps*eps/dt - eps*(FluxPos(j)+FluxVel(j))*invdx*invdv
                ! Gn(1,j) = Gn(1,j) / (eps*eps/dt + 1.0_rp)
                momvGn = momvGn + dv*V(j)*Gn(1,j)
            end do
            
            ! 5) Compute the kinetic macro flux
            RhoFlux(1) = -inveps*momvGn

            ! Maybe attempt to be smart on memory management ???
            ! - Not recomputing some fluxes when a cell has a kinetic neighboor

        else ! >>> Use the limit model 
            ! do j = 1, Nv
            !     Gn(1,j) = zero
            ! end do
            ! Limit flux
            RhoFlux(1) = m2*invdx*(rho(1)-rho(Nx)) - Ehalf(1)*rhohalf(1)
        end if

        do i = 2, Nx-1
            if(isKinetic(i))then ! >>> Use the kinetic model
                ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\

                ! Only Check the first neighboors as sub-domains cannot be of size 1
                ! 1) Check the state of the left neighboor and adapt the value of Gl
                if(isKinetic(i-1))then
                    Gl = G(i-1,:) ! G_i-1.5
                else
                    Gl = zero ! Voir reconstruction ici ? /!\/!\/!\/!\
                end if
                ! 2) Check the state of the right neighboor and adapt the value of Gr
                if(isKinetic(i+1))then
                    Gr = G(i+1,:) ! G_i+0.5
                else
                    Gr = zero ! Voir reconstruction ici ? /!\/!\/!\/!\
                end if

                ! 3) Compute the kinetic micro flux
                ! 3.1) The velocity flux and moments needed for the position flux
                FluxVel(1) = (Ehm(i)*G(i,1) + Ehp(i)*G(i,2)&
                            +0.5_rp*Ehalf(i)*rhohalf(i)*(M(2)+M(1)))*dx ! Zero boundary flux in velocity
                phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(i,1) - Vm(1)*G(i,1) - Vp(1)*Gl(1))

                momphi = zero
                momphi = momphi + phi(1)*dv

                do j = 2, Nv-1
                    ! Directly compute the sum of the fluxes
                    ! Upwindflux
                    FluxVel(j) = (Ehm(i)*G(i,j+1) + Ehp(i)*G(i,j) - Ehm(i)*G(i,j) - Ehp(i)*G(i,j-1)&
                                +0.5_rp*Ehalf(i)*rhohalf(i)*(M(j+1)-M(j-1)))*dx
                    phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(i,j) - Vm(j)*G(i,j) - Vp(j)*Gl(j))
                    momphi = momphi + phi(j)*dv
                end do

                FluxVel(Nv) = (Ehm(i)*G(i,Nv-1) + Ehp(i)*G(i,Nv)&
                             +0.5_rp*Ehalf(i)*rhohalf(i)*(M(Nv)+M(Nv-1)))*dx ! Zero boundary flux in velocity
                phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(i,Nv) - Vm(Nv)*G(i,Nv) - Vp(Nv)*Gl(Nv))
                momphi = momphi + phi(Nv)*dv

                ! 3.2) The position flux
                do j = 1, Nv
                    ! Upwind flux
                    FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*(rho(i)-rho(i-1)) )*dv
                end do

                ! 4) Update Gn at interface i-0.5 for all j and compute its moment
                momvGn = zero

                do j = 1, Nv
                    Gn(i,j) = G(i,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                            *invdx*invdv*(FluxPos(j) + FluxVel(j))

                    ! Gn(i,j) = G(i,j)*eps*eps/dt - eps*(FluxPos(j)+FluxVel(j))*invdx*invdv
                    ! Gn(i,j) = Gn(i,j) / (eps*eps/dt + 1.0_rp)

                    momvGn = momvGn + dv*V(j)*Gn(i,j)
                end do
                
                ! 5) Compute the kineticVm(1)*Gr(1) + Vp(1)*G(i,1) macro flux
                RhoFlux(i) = -inveps*momvGn
                ! Maybe attempt to be smart on memory management ???
                ! - Not recomputing some fluxes when a cell has a kinetic neighboor

            else ! >>> Use the limit model 
                ! do j = 1, Nv
                !     Gn(i,j) = zero
                ! end do
                ! Limit flux
                RhoFlux(i) = m2*invdx*(rho(i)-rho(i-1)) - Ehalf(i)*rhohalf(i)
            end if
        end do

        ! Right boundary, i = Nx, interface Nx-0.5
        if(isKinetic(Nx))then ! >>> Use the kinetic model
            ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\

            ! Only Check the first neighboors as sub-domains cannot be of size 1
            ! 1) Check the state of the left neighboor and adapt the value of Gl
            if(isKinetic(Nx-1))then
                Gl = G(Nx-1,:) ! G_i-1.5
            else
                Gl = zero
            end if
            ! 2) Check the state of the right neighboor and adapt the value of Gr
            if(isKinetic(1))then
                Gr = G(1,:) ! G_i+0.5
            else
                Gr = zero
            end if

            ! 3) Compute the kinetic micro flux
            ! 3.1) The velocity flux and moments needed for the position flux
            FluxVel(1) = (Ehm(Nx)*G(Nx,1) + Ehp(Nx)*G(Nx,2)&
                        +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(2)+M(1)))*dx ! Zero boundary flux in velocity
            phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(Nx,1) - Vm(1)*G(Nx,1) - Vp(1)*Gl(1))
            momphi = zero
            momphi = momphi + phi(1)*dv

            do j = 2, Nv-1
                ! Directly compute the sum of the fluxes
                ! Upwindflux
                FluxVel(j) = (Ehm(Nx)*G(Nx,j+1) + Ehp(Nx)*G(Nx,j) - Ehm(Nx)*G(Nx,j) - Ehp(Nx)*G(Nx,j-1)&
                            +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(j+1)-M(j-1)))*dx
                phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(Nx,j) - Vm(j)*G(Nx,j) - Vp(j)*Gl(j))
                momphi = momphi + phi(j)*dv
            end do

            FluxVel(Nv) = (Ehm(Nx)*G(Nx,Nv-1) + Ehp(Nx)*G(Nx,Nv)&
                        +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(Nv)+M(Nv-1)))*dx ! Zero boundary flux in velocity
            phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(Nx,Nv) - Vm(Nv)*G(Nx,Nv) - Vp(Nv)*Gl(Nv))
            momphi = momphi + phi(Nv)*dv

            ! 3.2) The position flux
            do j = 1, Nv
                ! Upwind flux
                FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*(rho(Nx)-rho(Nx-1)) )*dv
            end do

            ! 4) Update Gn at interface i-0.5 for all j and compute its moment
            momvGn = zero
            do j = 1, Nv
                Gn(Nx,j) = G(Nx,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                        *invdx*invdv*(FluxPos(j) + FluxVel(j))

                ! Gn(Nx,j) = G(Nx,j)*eps*eps/dt - eps*(FluxPos(j)+FluxVel(j))*invdx*invdv
                ! Gn(Nx,j) = Gn(Nx,j) / (eps*eps/dt + 1.0_rp)

                momvGn = momvGn + dv*V(j)*Gn(Nx,j)
            end do

            ! 5) Compute the kinetic macro flux
            RhoFlux(Nx) = -inveps*momvGn

            ! Maybe attempt to be smart on memory management ???
            ! - Not recomputing some fluxes when a cell has a kinetic neighboor
        else ! >>> Use the limit model 
            ! do j = 1, Nv
            !     Gn(Nx,j) = zero
            ! end do
            ! Limit flux
            RhoFlux(Nx) = m2*invdx*(rho(Nx)-rho(Nx-1)) - Ehalf(Nx)*rhohalf(Nx)
        end if
        ! Flux on rho at interface Nx+0.5 identified with 0.5 by periodicity
        RhoFlux(Nx+1) = RhoFlux(1)
        
        ! ----------------------------------------------------------------
        ! --------------- Second, Update the density rho -----------------
        ! ----------------------------------------------------------------
        do i = 1, Nx
            rho_n(i) = rho(i) + dtsdx*(RhoFlux(i+1) - RhoFlux(i))
        end do

    end subroutine OneTimeStep_Hybrid_MM
!
    subroutine OneTimeStep_Hybrid_MM_SG()
        implicit none
        integer  :: i, j
        real(rp), dimension(1:Nx+1) :: RhoFlux, Jflux
        real(rp), dimension(1:Nv) :: Gl, Gr, FluxVel, FluxPos, phi
        real(rp) :: momphi, momvGn, B1, B2

        B1 = exp(Efield_Cos(dx)/m2)
        B2 = 1.0_rp/(1.0_rp-B1)
        do i = 1, Nx
            Jflux(i) = Ehalf(i)*B2*(rho(i+1)-rho(i)*B1)
        end do
        Jflux(Nx+1) = Jflux(1)

        ! ----------------------------------------------------------------
        ! ---- First, deal with the pertubation g  and fluxes for rho ----
        ! ----------------------------------------------------------------
        ! Left Boundary, i = 1, interface 0.5
        if(isKinetic(1))then
            ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\
            if(isKinetic(Nx))then
                Gl = G(Nx,:) ! G_i-1.5
            else
                Gl = zero
            end if
            if(isKinetic(2))then
                Gr = G(2,:) ! G_i+0.5
            else
                Gr = zero 
            end if

            FluxVel(1) = (Ehm(1)*G(1,1) + Ehp(1)*G(1,2))*dx ! Zero boundary flux in velocity
            phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(1,1) - Vm(1)*G(1,1) - Vp(1)*Gl(1))
            momphi = zero
            momphi = momphi + phi(1)*dv
            do j = 2, Nv-1
                FluxVel(j) = (Ehm(1)*G(1,j+1) + Ehp(1)*G(1,j) - Ehm(1)*G(1,j) - Ehp(1)*G(1,j-1))*dx
                phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(1,j) - Vm(j)*G(1,j) - Vp(j)*Gl(j))
                momphi = momphi + phi(j)*dv
            end do
            FluxVel(Nv) = (Ehm(1)*G(1,Nv-1) + Ehp(1)*G(1,Nv))*dx ! Zero boundary flux in velocity
            phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(1,Nv) - Vm(Nv)*G(1,Nv) - Vp(Nv)*Gl(Nv))
            momphi = momphi + phi(Nv)*dv

            do j = 1, Nv
                FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*Jflux(1))*dv
            end do

            momvGn = zero
            do j = 1, Nv
                Gn(1,j) = G(1,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                        *invdx*invdv*(FluxPos(j) + FluxVel(j))
                momvGn = momvGn + dv*V(j)*Gn(1,j)
            end do
            RhoFlux(1) = -inveps*momvGn

        else
            RhoFlux(1) = Jflux(1)
        end if

        do i = 2, Nx-1
            if(isKinetic(i))then
                ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\
                if(isKinetic(i-1))then
                    Gl = G(i-1,:) ! G_i-1.5
                else
                    Gl = zero
                end if
                ! 2) Check the state of the right neighboor and adapt the value of Gr
                if(isKinetic(i+1))then
                    Gr = G(i+1,:) ! G_i+0.5
                else
                    Gr = zero
                end if

                FluxVel(1) = (Ehm(i)*G(i,1) + Ehp(i)*G(i,2))*dx ! Zero boundary flux in velocity
                phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(i,1) - Vm(1)*G(i,1) - Vp(1)*Gl(1))

                momphi = zero
                momphi = momphi + phi(1)*dv

                do j = 2, Nv-1
                    FluxVel(j) = (Ehm(i)*G(i,j+1) + Ehp(i)*G(i,j) - Ehm(i)*G(i,j) - Ehp(i)*G(i,j-1))*dx
                    phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(i,j) - Vm(j)*G(i,j) - Vp(j)*Gl(j))
                    momphi = momphi + phi(j)*dv
                end do

                FluxVel(Nv) = (Ehm(i)*G(i,Nv-1) + Ehp(i)*G(i,Nv))*dx ! Zero boundary flux in velocity
                phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(i,Nv) - Vm(Nv)*G(i,Nv) - Vp(Nv)*Gl(Nv))
                momphi = momphi + phi(Nv)*dv

                do j = 1, Nv
                    FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*Jflux(i))*dv
                end do

                momvGn = zero
                do j = 1, Nv
                    Gn(i,j) = G(i,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                            *invdx*invdv*(FluxPos(j) + FluxVel(j))
                    momvGn = momvGn + dv*V(j)*Gn(i,j)
                end do
                RhoFlux(i) = -inveps*momvGn
            else
                RhoFlux(i) = Jflux(i)
            end if
        end do

        if(isKinetic(Nx))then ! >>> Use the kinetic model
            ! /!\/!\/!\ Note that we deal with G_i-0.5 /!\/!\/!\
            if(isKinetic(Nx-1))then
                Gl = G(Nx-1,:) ! G_i-1.5
            else
                Gl = zero
            end if
            if(isKinetic(1))then
                Gr = G(1,:) ! G_i+0.5
            else
                Gr = zero
            end if
            FluxVel(1) = (Ehm(Nx)*G(Nx,1) + Ehp(Nx)*G(Nx,2)&
                        +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(2)+M(1)))*dx ! Zero boundary flux in velocity
            phi(1) = (Vm(1)*Gr(1) + Vp(1)*G(Nx,1) - Vm(1)*G(Nx,1) - Vp(1)*Gl(1))
            momphi = zero
            momphi = momphi + phi(1)*dv

            do j = 2, Nv-1
                FluxVel(j) = (Ehm(Nx)*G(Nx,j+1) + Ehp(Nx)*G(Nx,j) - Ehm(Nx)*G(Nx,j) - Ehp(Nx)*G(Nx,j-1)&
                            +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(j+1)-M(j-1)))*dx
                phi(j) = (Vm(j)*Gr(j) + Vp(j)*G(Nx,j) - Vm(j)*G(Nx,j) - Vp(j)*Gl(j))
                momphi = momphi + phi(j)*dv
            end do

            FluxVel(Nv) = (Ehm(Nx)*G(Nx,Nv-1) + Ehp(Nx)*G(Nx,Nv)&
                        +0.5_rp*Ehalf(Nx)*rhohalf(Nx)*(M(Nv)+M(Nv-1)))*dx ! Zero boundary flux in velocity
            phi(Nv) = (Vm(Nv)*Gr(Nv) + Vp(Nv)*G(Nx,Nv) - Vm(Nv)*G(Nx,Nv) - Vp(Nv)*Gl(Nv))
            momphi = momphi + phi(Nv)*dv

            do j = 1, Nv
                FluxPos(j) = (phi(j) - M(j)*momphi + V(j)*M(j)*(rho(Nx)-rho(Nx-1)) )*dv
            end do

            momvGn = zero
            do j = 1, Nv
                Gn(Nx,j) = G(Nx,j)*exp(-dtseps2) - eps*(1-exp(-dtseps2))&
                        *invdx*invdv*(FluxPos(j) + FluxVel(j))
                momvGn = momvGn + dv*V(j)*Gn(Nx,j)
            end do
            RhoFlux(Nx) = -inveps*momvGn
        else
            RhoFlux(Nx) = Jflux(Nx)
        end if
        ! Flux on rho at interface Nx+0.5 identified with 0.5 by periodicity
        RhoFlux(Nx+1) = RhoFlux(1)
        
        ! ----------------------------------------------------------------
        ! --------------- Second, Update the density rho -----------------
        ! ----------------------------------------------------------------
        do i = 1, Nx
            rho_n(i) = rho(i) + dtsdx*(RhoFlux(i+1) - RhoFlux(i))
        end do
    end subroutine OneTimeStep_Hybrid_MM_SG
!
    ! -------------------------------------
    ! ------------- Solvers ---------------
    ! -------------------------------------
!
    subroutine Solve_Hybrid_MM()
        implicit none
        integer  :: i, j, iter = 0, frames = 0
        real(rp) :: date = zero
        logical  :: UpdateG 

        ! --- Save Initial Datas ----

        if(Coupling)then
            call Comp_indicators()
        end if
        ! call Comp_indicators()

        call Save_distribution(iter, frames)
        call Save_density(iter, frames)
        call Save_StateAndMass(iter, frames)
        call Save_indicator(iter, frames)
        frames = frames+1

        do while (date < T)! .and. iter < 1)
            ! --- Advance the solution ----
            call OneTimeStep_Hybrid_MM()
            ! call OneTimeStep_Hybrid_MM_SG()
            ! --- Coupling machinery ----
            if(Coupling)then
                ! call Comp_indicators()
                call UpdateState()
            end if
            call Comp_indicators()

            ! Updating of G:
            do i = 1, Nx
                ! Kinetic to kinetic --| 
                !                      |--> G is updated via MM
                ! Kinetic to fluid ----|
                if(isKinetic(i))then
                    do j = 1, Nv
                        G(i,j) = Gn(i,j)
                    end do
                end if
                ! Fluid to kinetic, G is updated at 0.0
                if((.not.isKinetic(i)) .and. isKinetic_n(i))then
                    do j = 1, Nv
                        G(i,j) = zero
                    end do
                end if
                ! Fluid to fluid, do nothing on G

                ! Update the state
                isKinetic(i) = isKinetic_n(i)
            end do

            ! do i = 1, Nx
            !     do j = 1, Nv
            !         G(i,j) = Gn(i,j)
            !     end do
            !     isKinetic(i) = isKinetic_n(i)
            ! end do

            ! ! --- Reconstruct the distribution : ----
            ! ! F = RHO*M + G
            ! do j = 1, Nv
            !     do i = 1, Nx
            !         F(i,j) = rho_n(i)*M(j) + 0.5_rp*(G(i,j)+G(i+1,j))
            !     end do 
            !     F(Nx,j) = rho_n(Nx)*M(j) + 0.5_rp*(G(Nx,j)+G(1,j))
            ! end do

            ! Mass, update rho, update rho at interfaces
            mass = zero
            mass = mass + dx*rho_n(1)
            rho(1) = rho_n(1)
            rhohalf(1) = 0.5_rp*(rho_n(Nx) + rho_n(1))
            do i = 2, Nx
                mass = mass + dx*rho_n(i)
                rho(i) = rho_n(i)
                rhohalf(i) = 0.5_rp*(rho_n(i-1) + rho_n(i))
            end do 

            date = date + dt  
            iter = iter + 1 

            ! --- Save Datas ----
            if(iter==freq*frames) then
                call Save_distribution(iter, frames)
                call Save_density(iter, frames)
                call Save_StateAndMass(iter, frames)
                call Save_indicator(iter, frames)
                frames = frames+1
            end if
        end do
    end subroutine Solve_Hybrid_MM
!
    subroutine Solve_Limit_SG()
        implicit none
        real(rp), dimension(1:Nx+1) :: Jflux
        integer  :: i, iter = 0, frames = 0
        real(rp) :: date = zero, B1, B2

        ! --- Save Initial Datas ----
        if(mod(iter, freq)==0) then
            call Save_density(iter, frames)
            frames = frames+1
        end if

        do while (date <= T)

            ! FLUX AT i-0.5
            B1 = exp(-Ehalf(1)*dx/m2)
            B2 = 1.0_rp/(B1-1.0_rp)
            Jflux(1) = Ehalf(1)*B2*(rho(Nx)-rho(1)*B1)
            do i = 2, Nx
                B1 = exp(-Ehalf(i)*dx/m2)
                B2 = 1.0_rp/(B1-1.0_rp)
                Jflux(i) = Ehalf(i)*B2*(rho(i-1)-rho(i)*B1)
            end do
            Jflux(Nx+1)=Jflux(1)

            do i = 1, Nx
                rho_n(i) = rho(i) + dtsdx*m2*(Jflux(i+1) - Jflux(i))
                rho(i) = rho_n(i)
            end do

            ! do i = 1, Nx
            !     rho(i) = rho_n(i)
            ! end do

            ! ! ---- Left periodic BC ----
            ! B1 = exp(-Ehalf(1)*dx/m2)
            ! B2 = 1.0_rp/(B1-1.0_rp)
            ! rho_n(1) = rho(1) + m2*dtsdx*(Ehalf(2)*B2*(rho(2)*B1-rho(1))&
            !                             - Ehalf(1)*B2*(rho(1)*B1-rho(Nx)))
            ! ! ---- Interior of the domain ----
            ! do i = 2, Nx-1
            !     B1 = exp(-Efield_Cos(dx)/m2)
            !     B2 = 1.0_rp/(B1-1.0_rp)
            !     rho_n(i) = rho(i) + m2*dtsdx*(Ehalf(1)*B2*(rho(i+1)*B1-rho(i))&
            !                                 - Ehalf(2)*B2*(rho(i)*B1-rho(i-1)))
            ! end do
            ! ! ---- Right periodic BC ----
            ! B1 = exp(-Efield_Cos(dx)/m2)
            ! B2 = 1.0_rp/(B1-1.0_rp)
            ! rho_n(Nx) = rho(Nx) + m2*dtsdx*(Ehalf(1)*B2*(rho(1)*B1-rho(Nx))&
            !                               - Ehalf(2)*B2*(rho(Nx)*B1-rho(Nx-1)))
            ! do i = 1, Nx
            !     ! n --> n+1
            !     rho(i) = rho_n(i)
            ! end do

            date = date + dt  
            iter = iter + 1 

            ! ---- Save Datas ----
            if(mod(iter, freq)==0) then
                call Save_density(iter, frames)
                frames = frames+1
            end if
        end do
    end subroutine Solve_Limit_SG
!
    subroutine Solve_Limit()
        implicit none
        integer  :: i, iter = 0, frames = 0
        real(rp) :: date = zero

        ! --- Save Initial Datas ----
        if(mod(iter, freq)==0) then
            call Save_density(iter, frames)
            frames = frames+1
        end if

        do while (date <= T)
            ! ---- Left periodic BC ----
            rho_n(1) = rho(1) + m2*dtsdx*invdx*(rho(2)-2.0_rp*rho(1)+rho(Nx))&
                            - dtsdx*(Ehalf(2)*rhohalf(2)-Ehalf(1)*rhohalf(1))
            ! ---- Interior of the domain ----
            do i = 2, Nx-1
                rho_n(i) = rho(i) + m2*dtsdx*invdx*(rho(i+1)-2.0_rp*rho(i)+rho(i-1))&
                            - dtsdx*(Ehalf(i+1)*rhohalf(i+1)-Ehalf(i)*rhohalf(i))
            end do
            ! ---- Right periodic BC ----
            rho_n(Nx) = rho(Nx) + m2*dtsdx*invdx*(rho(1)-2.0_rp*rho(Nx)+rho(Nx-1))&
                            - dtsdx*(Ehalf(1)*rhohalf(1)-Ehalf(Nx)*rhohalf(Nx))
            ! ---- Compute the density at interfaces ----
            call Comp_rhohalf()
            do i = 1, Nx
                ! n --> n+1
                rho(i) = rho_n(i)
                rhohalf(i) = rhohalf_n(i)
            end do
            date = date + dt  
            iter = iter + 1 

            ! ---- Save Datas ----
            if(mod(iter, freq)==0) then
                call Save_density(iter, frames)
                frames = frames+1
            end if
        end do
    end subroutine Solve_Limit
end module hybridsolver