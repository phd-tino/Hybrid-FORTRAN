! Laidin Tino, Mars 2021
program volumes_finis
!
!$ use omp_lib
    use numerics
    use io
    use hybridsolver
!  
    implicit none
    real(rp) :: tmp
    real(rp) :: start, finish, runtime
!
    ! Parallel
    integer :: nbthds, mythd
!  
    write(6,*) "|========================================|"
    write(6,*) "|       Hybrid kinetic/fluid solver      |"
    write(6,*) "|========================================|"
!  
    write(6,*) "|----------------------------------------|"
    write(6,*) "|      Which solver to use ?             |"
    write(6,*) "| 1 --> Micro Macro                      |"
    write(6,*) "| 2 --> Limit scheme                     |"
    read(5,*) Method
    if(Method==1)then
        write(6,*) "|----------------------------------------|"
        write(6,*) "|      Enable Coupling ?                 |"
        write(6,*) "| 1 --> Yes                              |"
        write(6,*) "| 2 --> No                               |"
        read(5,*) tmp
        write(6,*) "|----------------------------------------|"
        if(tmp==1)then
            Coupling = .true.
            Output = "../output/Hybrid/"
            write(6,*) "|           Coupling enabled             |"
        else
            Coupling = .false.
            Output = "../output/FullKin/"
        end if
    else
        Output = "../output/Limit/"
    end if

!  
    datafile = "data.dat"
    call ReadData(datafile)
    write(6,*) "|----------------------------------------|"
    write(6,*) "|                  DATA                  |"
    write(6,*) "|----------------------------------------|"
    write(6,'(A,ES21.6,A)') " | xstar ", xstar, "            |"
    write(6,'(A,ES21.6,A)') " | vstar ", vstar, "            |"
    write(6,'(A,ES21.6,A)') " | T     ", T, "            |"
    write(6,'(A,ES21.6,A)') " | eps   ", eps, "            |"
    write(6,*) "| Nx    ", Nx
    write(6,*) "| Nv    ", Nv
    write(6,'(A,ES21.6,A)') " | C     ", C, "            |"
    write(6,'(A,ES21.6,A)') " | eta0  ", eta0, "            |"
    write(6,'(A,ES21.6,A)') " | delta0", delta0, "            |"
!
!$OMP parallel default(none) &
!$OMP shared(nbthds, Output, Method, runtime) &
!$OMP private(mythd, start, finish)
!
!$OMP master
!$ nbthds = OMP_GET_NUM_THREADS()
!$ write(6,*) "|----------------------------------------|"
!$ write(6, '(A,I4)') ' | Parallel execution, nbthds = ', nbthds
runtime = zero
nbthds = 1
!$OMP end master
!$ mythd = OMP_GET_THREAD_NUM()
!
    if(Method==1)then

        !$OMP single
        call allocate_MM()
        !$OMP end single

        call init_MM()

        call cpu_time(start)
        !$ start = omp_get_wtime()

        call Solve_Hybrid_MM()

        call cpu_time(finish)
        !$ finish = omp_get_wtime()
        runtime = finish-start
        !$OMP barrier

        !$OMP master  
        write(6,*) "|----------------------------------------|"
        write(6, '(A,ES21.6,A)') " | Runtime:", runtime, "sec"  
        call deallocate_MM()
        !$OMP end master

    else if(Method==2)then

        !$OMP single
        call allocate_Limit()
        !$OMP end single

        call init_Limit()

        call cpu_time(start)
        !$ start = omp_get_wtime()

        call Solve_Limit()
        ! call Solve_Limit_SG()

        call cpu_time(finish)
        !$ finish = omp_get_wtime()
        runtime = finish-start
        !$OMP barrier

        !$OMP single  
        write(6,*) "|----------------------------------------|"
        write(6, '(A,ES21.6,A)') " | Runtime:", runtime, "sec"  
        call deallocate_Limit()
        !$OMP end single
        
    else
        !$OMP single
        write(6,*) "/!\ Choice not recognized /!\"
        !$OMP end single
    end if
!$OMP barrier
!$OMP end parallel
!
    write(6,*) "|========================================|"
    write(6,*) "|          End of the programm           |"
    write(6,*) "|========================================|"
!  
end program volumes_finis