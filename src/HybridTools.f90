! Laidin Tino
module hybridtools
    !
    use numerics
    use solvertools
!    
    contains
!
    subroutine Comp_indicators()
        implicit none
        integer  :: i, j

        ! Compute the L2(d\gamma) norm in velocity of the perturbation g
        do i = 1, Nx
            if(isKinetic(i))then
                L2G(i) = zero
                do j = 1, Nv
                    L2G(i) = L2G(i) + G(i,j)*G(i,j)*dv*invEq(i,j)
                end do
                L2G(i) = sqrt(L2G(i))
            else
                L2G(i) = zero
            end if
        end do

        ! Compute the necessary derivatives of the density (1 to 4)
        call DerivativesDensity()
        ! Finally, compute the indicator
        do i = 1, Nx
            indicator(i) = abs( (-D4xRho(i)&
                +E(i)*(2.0_rp*D3xRho(i)-E(i)*D2xRho(i))&
                +D1xE(i)*(-3.0_rp*rho(i)*D1xE(i) - 5.0_rp*E(i)*D1xRho(i) + 6.0_rp*D2xRho(i))&
                +D2xE(i)*(-3.0_rp*rho(i)*E(i) + 5.0_rp*D1xRho(i))&
                +D3xE(i)*rho(i)) )*eps*eps
        end do

    end subroutine Comp_indicators
!
    subroutine UpdateState()
        implicit none
        integer :: i
        logical :: ChangeState, isSingle

        ! Update states based on the indicators

        !$OMP do
        do i = 1, Nx-1
            ChangeState = isKinetic(i)&
                        .and. (L2G(i) < delta0)&   !Check both interfaces of the cell
                        .and. (L2G(i+1) < delta0)&
                        .and. (indicator(i) <= eta0)
            ChangeState = ChangeState .or.&
                        (.not. isKinetic(i) .and. (indicator(i) > eta0))
            if(ChangeState) then
                isKinetic_n(i) = .not. isKinetic(i)
            else
                isKinetic_n(i) = isKinetic(i)
            end if
        end do
        !$OMP end do

        ChangeState = isKinetic(Nx)&
                    .and. (L2G(Nx) < delta0)&   !Check both interfaces of the cell
                    .and. (L2G(1) < delta0)&
                    .and. (indicator(Nx) <= eta0)
        ChangeState = ChangeState .or.&
                    (.not. isKinetic(Nx) .and. (indicator(Nx) > eta0))
        if(ChangeState) then
            isKinetic_n(Nx) = .not. isKinetic(Nx)
        else
            isKinetic_n(Nx) = isKinetic(Nx)
        end if

        ! Finalize states update by enforcing that there are no single FLUID cells
        ! We give priority to precision and therefore prefer to switch a single
        ! fluid cell to kinetic instead of the other way arround.
        
        ! Left boundary
        if(.not. isKinetic_n(1))then ! If the cell is fluid, check its neighbours ...
            if(isKinetic_n(Nx) .and. isKinetic_n(2))then ! ... if isolated, set it to kinetic
                isKinetic_n(1) = .true.
            else ! else, it keeps its new state
                isKinetic_n(1) = .false.
            end if
        end if

        ! Core of the domain
        !$OMP do
        do i = 2, Nx-1
            if(.not. isKinetic_n(i))then ! If the cell is fluid, check its neighbours ...
                if(isKinetic_n(i-1) .and. isKinetic_n(i+1))then ! ... if isolated, set it to kinetic
                    isKinetic_n(i) = .true.
                else ! else, it keeps its new state
                    isKinetic_n(i) = .false.
                end if
            end if
        end do
        !$OMP end do

        ! Right boundary
        if(.not. isKinetic_n(Nx))then ! If the cell is fluid, check its neighbours ...
            if(isKinetic_n(Nx-1) .and. isKinetic_n(1))then ! ... if isolated, set it to kinetic
                isKinetic_n(Nx) = .true.
            else ! else, it keeps its new state
                isKinetic_n(Nx) = .false.
            end if
        end if

        ! ! Simple update
        ! do i = 1, Nx
        !     isKinetic(i) = isKinetic_n(i)
        ! end do

        ! ! Finalize states update by enforcing that there are no single Kinetic cells
        ! ! We give priority to speed and therefore prefer to switch a single
        ! ! kinetic cell to fluid instead of the other way arround.
        
        ! ! Left boundary
        ! if(isKinetic_n(1))then ! If the cell is fluid, check its neighbours ...
        !     if(.not.isKinetic_n(Nx) .and. .not.isKinetic_n(2))then ! ... if isolated, switch
        !         isKinetic(1) = .false.
        !     else ! else, it keeps its new state
        !         isKinetic(1) = .true.
        !     end if
        ! end if

        ! ! Core of the domain
        ! !$OMP do
        ! do i = 2, Nx-1
        !     if(isKinetic_n(i))then ! If the cell is fluid, check its neighbours ...
        !         if(.not.isKinetic_n(i-1) .and. .not.isKinetic_n(i+1))then ! ... if isolated, switch
        !             isKinetic(i) = .false.
        !         else ! else, it keeps its new state
        !             isKinetic(i) = .true.
        !         end if
        !     end if
        ! end do
        ! !$OMP end do

        ! ! Right boundary
        ! if(isKinetic_n(Nx))then ! If the cell is fluid, check its neighbours ...
        !     if(.not.isKinetic_n(Nx-1) .and. .not.isKinetic_n(1))then ! ... if isolated, switch
        !         isKinetic(Nx) = .false.
        !     else ! else, it keeps its new state
        !         isKinetic(Nx) = .true.
        !     end if
        ! end if

    end subroutine UpdateState
!
end module hybridtools